import axios from "axios";

function sendRequest(endPoint, params = {}) {
  return new Promise(async (resolve, reject) => {
    let response = await axios.get(endPoint, {
      params: params,
    });
    if (response.status == 200 || 201) {
      resolve(response.data);
    } else {
      reject("Unkown Error");
    }
  });
}

export default sendRequest;
