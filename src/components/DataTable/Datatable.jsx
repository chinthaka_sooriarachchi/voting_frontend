import React, { forwardRef } from 'react';
import MaterialTable from 'material-table';
import {
    AddBox,
    ArrowDownward,
    Check,
    ChevronLeft,
    ChevronRight,
    Clear,
    DeleteOutline,
    Edit,
    FilterList,
    FirstPage,
    LastPage,
    Remove,
    SaveAlt,
    Search,
    ViewColumn
} from "@material-ui/icons";
import SendRequest from '../../rest-api';
import moment from 'moment';

const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};

const columns = [
    { title: 'ID', field: 'id'},
    { title: 'MSISDN', field: 'msisdn' },
    { title: 'Performer ID', field: 'performer_id'},
    { title: 'Voted Date Time', field: 'created_at', render: row => { return moment.utc(row.created_at).format('YYYY/MM/DD HH:mm:ss') } }
]

const tableRef = React.createRef();

class DataTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tableOptions: {
                search: false,
                pageSize: 10,
                pageSizeOptions: [10, 20, 50, 100],
                debounceInterval: 250,
                isLoading: true,
                showTitle: false,
                tableLayout: 'auto'
            },
            filters: props.filters
        }
    }
    render() {
        return (<MaterialTable 
            tableRef={tableRef}
            columns={columns}
            options={this.state.tableOptions}
            icons={tableIcons}
            data={query =>
                new Promise(async (resolve, reject) => {
                    let res = await this.fethData(query);
                    resolve(res);
                })
            }
        />);
    }
    fethData = async (query) => {
        let orderBy = "";
        let orderDir = "";
        if (query.orderBy) {
            orderDir = query.orderDirection;
            orderBy = query.orderBy.field;
        } else {
            orderDir = "asc";
            orderBy = "id";
        }
        let res = await SendRequest(process.env.REACT_APP_ENDPOINT_VOTES, {
            page: query.page + 1,
            limit: query.pageSize,
            orderBy: orderBy,
            direction: orderDir,
            performerId: this.state.filters.performerId,
            msisdn: this.state.filters.msisdn,
            dateRange: this.state.filters.dateRange,
        });
        return {
            data: res.data,
            page: res.page - 1,
            totalCount: res.total,
        }
    }
    reSearchData = () => {
        tableRef.current && tableRef.current.onQueryChange()

    }
}

export default DataTable;