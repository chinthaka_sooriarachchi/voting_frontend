import React from "react";
// react-bootstrap components
import { Card, Container, Row, Col } from "react-bootstrap";
import { Bar, Pie } from "react-chartjs-2";

import SendRequest from '../rest-api';

class Dashboard extends React.Component {
	state = {
		performerData: {
			label: [],
			data: []
		},
		overallData: {
			label: [],
			data: []
		}
	}
	constructor(props) {
		super(props);
	}
	async componentDidMount() {
		await this.getPerformerVotes();
		await this.getOverallVotes();
	}
	render() {
		let performerGraph = {
			data: {
				labels: this.state.performerData.label,
				datasets: [
					{
						label: "# of Votes",
						data: this.state.performerData.data,
						backgroundColor: [
							"rgba(255, 165, 0, 0.2)",
							"rgba(107, 142, 35, 0.2)",
							"rgba(255, 20, 147, 0.2)",
							"rgba(75, 192, 192, 0.2)",
							"rgba(153, 102, 255, 0.2)",
							"rgba(112, 191, 255, 0.2)",
							"rgba(255, 221, 64, 0.2)",
							"rgba(54, 162, 235, 0.2)",
							"rgba(144, 238, 144, 0.2)",
							"rgba(244,164,96, 0.2)",
						],
						borderColor: [
							"rgba(255, 165, 0, 1)",
							"rgba(107, 142, 35, 1)",
							"rgba(255, 20, 147, 1)",
							"rgba(75, 192, 192, 1)",
							"rgba(153, 102, 255, 1)",
							"rgba(112, 191, 255, 1)",
							"rgba(255, 221, 64, 1)",
							"rgba(54, 162, 235, 1)",
							"rgba(144, 238, 144, 0, 1)",
							"rgba(244,164,96, 1)",
						],
						borderWidth: 1,
					},
				],
			},
			options: {
				elements: {
					bar: {
						borderWidth: 2,
					},
				},
				responsive: true,
				plugins: {
					legend: {
						position: "bottom",
					},
				},
			},
		}

		let overallVotesGraph = {
			labels: this.state.overallData.label,
			datasets: [
				{
					label: "# of Votes",
					data: this.state.overallData.data,
					backgroundColor: [
						"rgba(255, 165, 0, 0.2)",
						"rgba(107, 142, 35, 0.2)",
						"rgba(107, 206, 86, 0.2)",
						"rgba(75, 192, 192, 0.2)",
						"rgba(153, 102, 255, 0.2)",
						"rgba(112, 191, 255, 0.2)",
						"rgba(255, 221, 64, 0.2)",
						"rgba(54, 162, 235, 0.2)",
						"rgba(144, 238, 144, 0.2)",
						"rgba(244, 164, 96, 0.2)",
					],
					borderColor: [
						"rgba(255, 165, 0, 1)",
						"rgba(107, 142, 35, 1)",
						"rgba(107, 206, 86, 1)",
						"rgba(75, 192, 192, 1)",
						"rgba(153, 102, 255, 1)",
						"rgba(112, 191, 255, 1)",
						"rgba(255, 221, 64, 1)",
						"rgba(54, 162, 235, 1)",
						"rgba(144, 238, 144, 0, 1)",
						"rgba(244, 164, 96, 1)",
					],
					borderWidth: 1,
				},
			],
		};

		return (<Container fluid>
			<Row>
				<Col md="8">
					<Card>
						<Card.Header>
							<Card.Title as="h4">
								Contestent Votes
							</Card.Title>
							<p className="card-category">
								Per contestent votes
							</p>
						</Card.Header>
						<Card.Body>
							<Bar
								data={performerGraph.data}
								options={performerGraph.options}
							/>
						</Card.Body>
					</Card>
				</Col>
				<Col md="4">
					<Card>
						<Card.Header>
							<Card.Title as="h4">Overall Votings</Card.Title>
							<p className="card-category">
								Contestent votes percentage
							</p>
						</Card.Header>
						<Card.Body>
							<Pie data={overallVotesGraph} />
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>);
	}

	getPerformerVotes = async () => {
		let response = await SendRequest(process.env.REACT_APP_ENDPOINT_PERFORMER_COUNT);
		let labels = [];
		let data = [];
		response.data.forEach(element => {
			labels.push("P" + element.performer_id);
			data.push(element.count);
		});
		let currState = this.state;
		currState.performerData.data = data;
		currState.performerData.label = labels;
		this.setState(currState);
	}

	getOverallVotes = async () => {
		let response = await SendRequest(process.env.REACT_APP_ENDPOINT_PERCENTAGE_COUNT);
		let labels = [];
		let data = [];
		response.data.forEach(element => {
			labels.push("P" + element.performer_id);
			data.push(element.count);
		});
		let currState = this.state;
		currState.overallData.data = data;
		currState.overallData.label = labels;
		this.setState(currState);
	}
}

export default Dashboard;
