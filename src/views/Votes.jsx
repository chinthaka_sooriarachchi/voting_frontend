
import React from "react";
import { Button, Card, Form, Container, Row, Col } from "react-bootstrap";
import DataTable from '../components/DataTable/Datatable';
import DateRangePicker from 'react-bootstrap-daterangepicker';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-daterangepicker/daterangepicker.css';
import moment from "moment";
import SimpleReactValidator from 'simple-react-validator';

const mystyle = {
	color: "red",
};

export default class Votes extends React.Component {

	state = {
		filters: {
			performerId: "",
			msisdn: "",
			dateRange: ""
		},
		errorMessages: {
			performerId: ""
		}
	};

	constructor(props) {
		super(props);
		this.tableRef = React.createRef();
		this.validator = new SimpleReactValidator({
			autoForceUpdate: this,
			element: message => <div style={{ color: "red" }}>{message}</div>

		});
	}

	componentDidMount() {
		this.forceUpdate();
	}

	render() {
		const { filters } = this.state
		return (
			<Container fluid>
				<Row>
					<Col md="12">
						<Card className="strpied-tabled-with-hover">
							<Card.Header>
								<Card.Title as="h4">
									Search Contestent
								</Card.Title>
								<p className="card-category">
									Filter contestents
								</p>
							</Card.Header>
							<Card.Body>
								<Form onSubmit={this.handleSubmit}>
									<Row>
										<Col md="4">
											<Form.Group>
												<label>Performer ID</label>
												<Form.Control
													value={filters.performerId}
													name="performerId"
													onChange={this.handleChange}
													placeholder="Search by performer ID"
													type="text"
													onBlur={() => this.validator.showMessageFor('performer_id')}
												></Form.Control>
												{this.validator.message('performer_id', filters.performerId, 'numeric|max:10,num|min:0,num')}
											</Form.Group>
										</Col>
										<Col md="4">
											<Form.Group>
												<label>MSISDN</label>
												<Form.Control
													value={filters.msisdn}
													name="msisdn"
													onChange={this.handleChange}
													placeholder="Search by msisdn"
													type="text"
												></Form.Control>
											</Form.Group>
										</Col>
										<Col md="4">
											<Form.Group>
												<label>Date Range</label>
												<DateRangePicker
													initialSettings={{ autoUpdateInput: false, timePicker: true, timePicker24Hour: true }}
													onCallback={this.handleDateRangeCallback}
													onCancel={this.handleDateRangeCancel}
													onApply={this.handleDateRangeApply}
												>
													<Form.Control
														defaultValue={filters.dateRange || ""}
														name="dateRange"
														placeholder="Search by date range"
														type="text"
													></Form.Control>
												</DateRangePicker>

											</Form.Group>
										</Col>
									</Row>
									<Row>
										<Col>
											<Button
												className="btn-fill float-right"
												variant="primary"
												type="submit">
												Search
									</Button>
											<Button
												className="btn-fill float-right mr-2"
												type="button"
												variant="default"
												onClick={this.handleClear}
											>
												Clear
									</Button>
										</Col>
									</Row>
								</Form>
							</Card.Body>
						</Card>
						<Card>
							<Card.Header>
								<Card.Title as="h4">
									Search Contestent
								</Card.Title>
								<p className="card-category">
									Filter contestents
								</p>
							</Card.Header>
							<Card.Body>
								<DataTable ref={this.tableRef} filters={filters} />
							</Card.Body>
						</Card>
					</Col>
				</Row>
			</Container>
		);
	}

	handleDateRangeApply = (_event, picker) => {
		this.setDateRangeValues(picker.startDate, picker.endDate);
	}

	handleDateRangeCallback = (start, end, _label) => {
		this.setDateRangeValues(start, end);
	}

	setDateRangeValues = (start, end) => {
		let currState = this.state.filters;
		currState.dateRange = moment(start).format("YYYY/MM/DD HH:mm:ss") + " - " + moment(end).format("YYYY/MM/DD HH:mm:ss");
		this.setState({ filters: currState });
	}

	handleDateRangeCancel = (_event, _picker) => {
		let currState = this.state.filters;
		currState.dateRange = "";
		this.setState({ filters: currState });
	}

	handleClear = () => {
		let currState = this.state.filters;
		currState.performerId = "";
		currState.msisdn = "";
		currState.dateRange = "";
		this.setState({ filters: currState });
	}

	handleChange = (event) => {
		let performerIdErrMsg = ""
		if (event.target.name === "performerId") {
			if (event.target.value != "" && !/^\d*$/.test(event.target.value)) {
				performerIdErrMsg = <strong>Value must be a number</strong>;
			}
		}

		let currState = this.state;
		currState.filters[event.target.name] = event.target.value;
		currState.errorMessages.performerId = performerIdErrMsg

		this.setState(currState);
	}

	handleSubmit = (event) => {
		event.stopPropagation();
		event.preventDefault();
		if (this.validator.allValid()) {
			this.tableRef.current.reSearchData();
		} else {
			this.validator.showMessages();
			this.forceUpdate();
		}


	}
}
